const path = require("path");

const FOUNDATIONS_PRODUCT_MANAGER = "@cdybenko";
const FOUNDATIONS_ENGINEERING_MANAGER = "@samdbeckham";
const FOUNDATIONS_PRODUCT_DESIGN_MANAGER = "@chrismicek";

function getPlanningIssueTemplatePath() {
  return path.join(
    __dirname,
    "../.gitlab",
    "issue_templates",
    "foundations_planning_issue.md"
  );
}

function getNavigationUpdateTemplatePath() {
  return path.join(
    __dirname,
    "../.gitlab",
    "issue_templates",
    "foundations_navigation_update.md"
  );
}

exports.CURRENT_PROJECT = "14402567";
exports.NAVIGATION_UPDATE_TEMPLATE_PATH = getNavigationUpdateTemplatePath();

exports.GROUPS = [
  {
    name: "Foundations",
    assignees: [
      FOUNDATIONS_PRODUCT_MANAGER,
      FOUNDATIONS_PRODUCT_DESIGN_MANAGER,
      FOUNDATIONS_ENGINEERING_MANAGER,
    ],
    planningIssueTemplatePath: getPlanningIssueTemplatePath(),
    planningIssueLabels: ["group::foundations"],
  },
];

exports.DEFAULT_ISSUE_LABELS = [
  "Planning Issue",
  "devops::manage",
  "section::dev",
  "type::ignore",
];

exports.IS_DEV = process.env.NODE_ENV === "development";
