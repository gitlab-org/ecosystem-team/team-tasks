## Themes for this milestone :clipboard: 

1. Theme 1
1. Theme 2
1. Theme 3

See our [direction](https://about.gitlab.com/direction/manage/personal_productivity) page for long-term plans.

## Quick Reference :reminder_ribbon: 

- :calendar: Important dates:
  - This milestone runs from **yyyy-mm-dd - yyyy-mm-dd**.
- :reminder_ribbon: Issue Boards
  - [Issues by Workflow label](https://gitlab.com/groups/gitlab-org/-/boards/3871464?milestone_title=Started&label_name%5B%5D=group::personal+productivity)
  - [Issues by Milestone](https://gitlab.com/groups/gitlab-org/-/boards/7295775?label_name%5B%5D=group::personal+productivity)
- :gitlab-hero: Want to contribute?
  - See our [issues seeking community contributions](https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_asc&state=opened&label_name%5B%5D=group::foundations&label_name%5B%5D=Seeking%20community%20contributions&first_page_size=100)


## Validation & Design Track :mag_right:

Summary of key goals and plans

- [ ] Issue 1
- [ ] Issue 2
- [ ] Issue 3

## Build Track :tools:

Summary of key goals and plans

- [ ] Issue 1
- [ ] Issue 2
- [ ] Issue 3

## Planned Time Off :palm_tree:

Hey team! :wave: Please add your planned time off below for this release period.

_*Note, you do not need to update this table if you take unplanned time off, this is meant to be a quick snapshot of capacity for those who don't have access to capacity planning._

|Team Member|Time Off|
|---|---|
| @ameliabauerly |  |
| @jrushford |  |
| @leipert |  |
| @samdbeckham |  |
| @thaina.t |  |
| @thutterer |  |

/label ~"group::personal productivity" ~"Planning Issue" ~"type::ignore"